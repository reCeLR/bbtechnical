# BBTechnical
technical assessment for Backbase

ASSUMPTIONS 

Customers and accounts are stored elsewhere and can be passed to this program to populate arrays
error handling to ensure that one user has exactly one savings and one current account handled elsewhere
to simulate that for the purpose of this demonstration, dummy customer and account data has been added to the array to match the input file

if this was the case the customer-ID could be pulled from the file name and their current account and savings account could be searched for using that
for a slightly different implementation

assuming that there is negligible delay between customer submitting a transaction and the system checking account balances so transaction time does not change


TO RUN 

to compile Java program cd into /bbtechnical/src/bbtechnical/ and run "javac *.java"

then to run, return to the src folder (cd ../) and run "java bbtechnical/Main [input file location] [output folder location]"