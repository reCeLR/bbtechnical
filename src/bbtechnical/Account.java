package bbtechnical;
/**
 * account object to hold all universal account variables
 * @author Chris Richards
 *
 */
public class Account {
	
	/**
	 * unique ID for account holder
	 */
	private int CustomerID;
	
	/**
	 * Unique account identifier
	 */
	private int accountID;
	/**
	 * current balance of account
	 */
	private float balance = 0;
	
	/**
	 * constructor
	 * @param accountID
	 * @param balance
	 */
	public Account(int accountID, int CustomerID) {
		this.accountID = accountID;
	}
	/**
	 * 
	 * @return customers unique ID
	 */
	public int getCustomerID() {
		return CustomerID;
	}
	

	/**
	 * 
	 * @return unique account ID
	 */
	public int getAccountID() {
		return accountID;
	}
	/**
	 * 
	 * @return balance of account
	 */
	public float getBalance() {
		return balance;
	}
	/**
	 * set unique ID
	 * @param accountID the unique ID
	 */
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}
	
	/**
	 * set balance
	 * @param newBalance updated balance
	 */
	public void setBalance(float newBalance) {
		this.balance = newBalance;
	}

}
