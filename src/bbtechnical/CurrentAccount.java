package bbtechnical;
/**
 * Current Account object which inherits from Account.java
 * needing to hold overdraft which requires different functionality than savingsaccounts
 * @author Chris Richards
 *
 */

public class CurrentAccount extends Account{
	
	/**
	 * customers overdraft, initiated to 0
	 */
	private float overdraft = 0;

	public CurrentAccount(int accountID, int customerID) {
		super(accountID, customerID);
	}
	
	/**
	 * 
	 * @return current overdraft
	 */
	float getOverdraft() {
		return overdraft;
	}
	
	/**
	 * 
	 * @param newOverdraft new overdraft value
	 */
	void setOverdraft(float newOverdraft) {
		this.overdraft = newOverdraft;
	}

}
