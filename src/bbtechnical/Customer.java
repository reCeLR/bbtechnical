package bbtechnical;

/**
 * Customer object class 
 * @author Chris Richards
 *
 */

public class Customer{
	int customerID;
	Account savingsAcc;
	Account currentAcc;
	
	
	/**
	 * in a real system I imagine the customer and accounts would already exist so their IDs would already be unique, manually setting IDs here for testing
	 * @param customerID
	 */
	
	public Customer(int customerID) {
		this.customerID = customerID;
		this.savingsAcc = new SavingsAccount(789,customerID);
		this.currentAcc = new CurrentAccount(123,customerID);
		
	}
	
	
	
	/**
	 * 
	 * @return unique customerID
	 */
	public int getCustomerID() {
		return customerID;
	}
	/**
	 * 
	 * @return customers savingsAcc object
	 */
	public Account getSavingsAcc() {
		return savingsAcc;
	}
	/**
	 * customers currectAcc object
	 * @return
	 */
	public Account getCurrentAcc() {
		return currentAcc;
	}
	
	/**
	 * 
	 * @param newCustomerID
	 */
	public void setCustomerID(int newCustomerID) {
		this.customerID = newCustomerID;
	}
	/**
	 * 
	 * @param newSavingsAcc
	 */
	public void setSavingsID(Account newSavingsAcc) {
		this.savingsAcc = newSavingsAcc;
	}
	/**
	 * 
	 * @param newCurrentAcc 
	 */
	public void setCurrentID(Account newCurrentAcc) {
		this.currentAcc = newCurrentAcc;
	}
	
	
}
