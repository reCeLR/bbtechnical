package bbtechnical;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
/**
 * Main class to begin the process
 * in this implementation a dummy customer is added to match the IDs in the input file
 * 
 * in a real system this could be initialised by looping through a text file or passed from another part of the overall system
 * @author Chris Richards
 *
 */

public class Main {
	

	
	static ArrayList<Customer> customersArray = new ArrayList<Customer>();
	static ArrayList<Account> accountsArray = new ArrayList<Account>();

	public static void main(String[] args) throws ParseException {
		
		
		/**
		 * takes command line arguments for input file and output folder
		 */
		String inputPath = args[0];
		String outputPath = args[1];
		
		/**
		 * FOR TESTING IN IDE
		 */
		//String inputPath = "/home/celr/Personal-Work/bbtechnical/input_files/input1.csv";
		//String outputPath ="/home/celr/Personal-Work/bbtechnical/output_files/" ;
		
		/**
		 * test customer
		 */
		Customer cust1 = new Customer(1);
		customersArray.add(cust1);
		accountsArray.add(cust1.currentAcc);
		accountsArray.add(cust1.savingsAcc);
		//cust1.currentAcc.setBalance(5);
		//cust1.savingsAcc.setBalance(3);
	

		
		Transaction newTransactions = new Transaction();
		try {
			newTransactions.fileReader(inputPath,outputPath,accountsArray);
		} catch (IOException e) {
			e.printStackTrace();
		}
	

	}


	
	

}
