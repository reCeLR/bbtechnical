package bbtechnical;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Transaction Class which processes the required transactions from a cvs file
 * @author Chris Richards
 *
 */
public class Transaction {
	
	/**
	 * reads cvs file and adds values to string array
	 * @param inputFile location of input file
	 * @param outputPath location to save output file to be passed to fileWriter
	 * @param accountsArray
	 * @throws IOException
	 */
	public void fileReader(String inputFile, String outputPath,ArrayList<Account> accountsArray) throws IOException {

		BufferedReader br = null;
		String fileLine = "";
		String[] transaction = null;
		
		try {
			br = new BufferedReader(new FileReader(inputFile));
			while ((fileLine = br.readLine())!= null) {
				transaction = fileLine.split(",");
				processString(transaction,accountsArray,outputPath);
			}
		}catch(FileNotFoundException e){
			System.out.println("Cannot Read file");
			e.printStackTrace();
		}finally {
			br.close();
		}
		
	}
	/**
	 * takes the string array and splits into required variables
	 * @param transaction
	 * @param accountsArr
	 * @param outputPath output file location to be passed to fileWriter
	 * @throws IOException
	 */
	public void processString(String[] transaction,ArrayList<Account> accountsArr,String outputPath) throws IOException {
		
		int accountID = 0;
		String accountType = "";
		String dateTime = null;
		float transactionValue = 0;
		
		try {
			accountID = Integer.parseInt(transaction[0]);
			accountType = transaction[1];
			
			dateTime = transaction[3];
	
					
			transactionValue = Float.parseFloat(transaction[4]);
		}catch(Exception e) {
			System.out.println("ERROR: MALFORMED DATA IN INPUT FILE");
			e.printStackTrace();
		}
		
		CurrentAccount currAcc = null;
		SavingsAccount savAcc = null;
		
		
		
		
		boolean found = false;
		int accIter = 0;
		
		while(found == false) {

			if(accountsArr.get(accIter).getAccountID() == accountID) {
				if(accountsArr.get(accIter) instanceof CurrentAccount) {
					currAcc = (CurrentAccount) accountsArr.get(accIter);
					found = true;
				}else if(accountsArr.get(accIter) instanceof SavingsAccount) {
					savAcc = (SavingsAccount) accountsArr.get(accIter);
					found = true;
				}
			}
		
			accIter++;
		}
		
		int customerID=0;
		if(currAcc != null) {
			customerID = currAcc.getCustomerID();
			for(int x = 0 ; x < accountsArr.size();x++) {
				if(accountsArr.get(x).getCustomerID() == customerID && accountsArr.get(x) instanceof SavingsAccount) {
					savAcc = (SavingsAccount) accountsArr.get(x);
				}
			}
		}else if(savAcc != null) {
			customerID = savAcc.getCustomerID();
			for(int x = 0 ; x < accountsArr.size();x++) {
				if(accountsArr.get(x).getCustomerID() == customerID && accountsArr.get(x) instanceof CurrentAccount) {
					currAcc = (CurrentAccount) accountsArr.get(x);
				}
			}
		}
		
		if(accountType.equals("CURRENT")){
		
			processCurrentTransaction(customerID,dateTime, currAcc, savAcc,transactionValue,outputPath);
		}else if(accountType.equals("SAVINGS")){
			processSavingsTransaction(customerID,dateTime, currAcc, savAcc,transactionValue,outputPath);
		}
		
		
	}
	/**
	 * removes any amount needed from savings to cover transaction where possible before adding to overdraft
	 * @param customerID
	 * @param dateTime
	 * @param currAcc
	 * @param savAcc
	 * @param transactionValue
	 * @param outputPath output file location to be passed to fileWriter
	 * @throws IOException
	 */
	public void processCurrentTransaction(int customerID,String dateTime,CurrentAccount currAcc, SavingsAccount savAcc, float transactionValue,String outputPath) throws IOException {
		
		/**
		 *if statement covering the possible outcomes of a transaction
		 *namely customer doesnt have any savings
		 *the customer has some savings but not enough
		 *the customer had enough savings to cover the difference
		 *the customer has enough money in current account to cover the transaction
		 * 
		 */
		
		if (transactionValue < 0.0){ //if the transaction is negative
			if(currAcc.getBalance() + transactionValue < 0) { //customer does not have enough money in current account
				
				float balance = currAcc.getBalance() + transactionValue;
				
				if(savAcc.getBalance() >= Math.abs(balance)) { //customer had enough money in savings to cover the difference
					savAcc.setBalance(savAcc.getBalance() + balance);
					currAcc.setBalance(0);
					fileWriter(customerID,savAcc.getAccountID(),"SAVINGS","SYSTEM",dateTime,balance,outputPath);
					fileWriter(customerID,currAcc.getAccountID(),"CURRENT","ACCOUNT-HOLDER",dateTime, transactionValue,outputPath);
					
				}else if(savAcc.getBalance() > 0) { //customer has some money in savings but not enough to cover difference so extra is put into overdraft
					balance = balance + savAcc.getBalance();
					currAcc.setBalance(0);
					savAcc.setBalance(0);
					currAcc.setOverdraft(currAcc.getOverdraft() + balance);
					fileWriter(customerID,savAcc.getAccountID(),"SAVINGS","SYSTEM",dateTime,balance,outputPath);
					fileWriter(customerID,currAcc.getAccountID(),"CURRENT","ACCOUNT-HOLDER",dateTime, transactionValue,outputPath);
					
				} else if(savAcc.getBalance() == 0) { //customer has no money in savings so all difference is put into overdraft
					currAcc.setOverdraft(transactionValue);
					fileWriter(customerID,currAcc.getAccountID(),"CURRENT","ACCOUNT-HOLDER",dateTime, transactionValue,outputPath);
				}
				
				
			}else { //customer has enough money to pay for the transaction
				currAcc.setBalance(currAcc.getBalance() + transactionValue);
				
				fileWriter(customerID,currAcc.getAccountID(), "CURRENT", "ACCOUNT-HOLDER",dateTime,transactionValue,outputPath);
				
			}
		}else if(transactionValue >= 0.0) { // if the transaction is positive
			
			if (currAcc.getOverdraft() <0.0) { // if the customer has an overdraft that is paid off first
				currAcc.setBalance(currAcc.getOverdraft() + transactionValue);
				currAcc.setOverdraft(0);
				fileWriter(customerID,currAcc.getAccountID(),"CURRENT","ACCOUNT-HOLDER",dateTime, transactionValue,outputPath);
				
			}else {
				
				currAcc.setBalance(currAcc.getBalance() + transactionValue);
				fileWriter(customerID,currAcc.getAccountID(),"CURRENT","ACCOUNT-HOLDER",dateTime, transactionValue,outputPath);

			}
		}
		
		
	}
	
	public void processSavingsTransaction(int customerID,String dateTime,CurrentAccount currAcc, SavingsAccount savAcc, float transactionValue,String outputPath) throws IOException {
		
		if(transactionValue < 0.0) {
			if(savAcc.getBalance() >= Math.abs(transactionValue)){
				savAcc.setBalance(savAcc.getBalance() + transactionValue);
				fileWriter(customerID,savAcc.getAccountID(), "SAVINGS", "ACCOUNT-HOLDER",dateTime,transactionValue,outputPath);
	
			}else{
				System.out.println("not enough money");
			}
		
		
		}else if(transactionValue >= 0.0){
			savAcc.setBalance(savAcc.getBalance() + transactionValue);
			fileWriter(customerID,savAcc.getAccountID(), "SAVINGS", "ACCOUNT-HOLDER",dateTime,transactionValue,outputPath);

		}
	}
	/**
	 * writes to output file in the required format
	 * @param customerID
	 * @param accountID
	 * @param accountType
	 * @param initiatorType
	 * @param dateTime
	 * @param transactionValue
	 * @param outputPath output file location
	 * @throws IOException
	 */
	public void fileWriter(int customerID,int accountID,String accountType, String initiatorType, String dateTime, float transactionValue,String outputPath) throws IOException {
		
		String filename = "customer-" + customerID + "-ledger";
		String outputLocation = outputPath+"/"+filename+".csv";
		String outputLine = accountID+","+accountType+','+initiatorType+","+dateTime+","+transactionValue;
		
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputLocation,true));
		bw.write(outputLine);
		bw.newLine();
		bw.close();
	}
	


}
